package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/astaxie/beego/orm"
	"github.com/goombaio/namegenerator"
	_ "github.com/mattn/go-sqlite3"
	vault "github.com/olmax99/vaultgo"

	"github.com/beego/beego/v2/core/config"
)

func init() {
	// Create DB
	os.MkdirAll("./data/dev/", 0755)
	os.Create("./data/dev/vault.db")

	orm.RegisterDriver("sqlite3", orm.DRSqlite)
	orm.RegisterDataBase("default", "sqlite3", "./data/dev/default.db")
	orm.RegisterDataBase("vaultdb", "sqlite3", "./data/dev/vault.db")

}

// ------------------ Vault Resources----------------------

func main() {

	orm.RegisterModel(new(User), new(Profile))

	// TODO config + RunSyncDb to func initDb()
	// Load configs
	iniconf, err := config.NewConfig("ini", "conf/app.conf")
	if err != nil {
		fmt.Println(err)
	}

	db_boot, err := iniconf.String("db::beego_db_bootstrap")
	if err != nil {
		fmt.Println(err)
	}
	db_debug, err := iniconf.String("db::beego_db_debug")
	if err != nil {
		fmt.Println(err)
	}

	if db_debug == "true" {
		orm.Debug = true
	}

	// orm.RunSyncdb needs to run at every startup
	if db_boot == "true" {
		name := "vaultdb"
		force := true
		verbose := true
		err := orm.RunSyncdb(name, force, verbose)
		if err != nil {
			fmt.Println(err)
		}
	} else {
		name := "vaultdb"
		force := false
		verbose := false
		err := orm.RunSyncdb(name, force, verbose)
		if err != nil {
			fmt.Println(err)
		}
	}

	// Enable cmd: orm syncdb
	orm.RunCommand()

	// -----------------------Vault Transit ------------------------

	// Step 1: -----------Get Vault Client--------------------------
	vaddr, err := iniconf.String("vault::address")
	if err != nil {
		fmt.Println(err)
	}

	client, err := vault.NewClient(vaddr, vault.WithCaPath(""))
	if err != nil {
		log.Fatal("PANIC [-] Could not connect with Vault")
	}

	// Step 2: ----------- Check for token--------------------------
	if t := client.Token(); t == "" {
		log.Print("WARNING [*] No token found in environment. Try config..")
		v, err := iniconf.String("vault::token")
		if err != nil {
			fmt.Println(err)
		}
		if v == "" {
			log.Fatal("PANIC [*] No token provided")

		} else {
			client.SetToken(v)
		}
	}

	log.Printf("INFO [*] yae... %#v", client)

	// Step 3: ----------- Encrypt String and Write to DB----------
	trans := client.Transit()
	// NOTE: This action will already create a new transit-key
	err = trans.Create("beego-key", &vault.TransitCreateOptions{})
	if err != nil {
		log.Printf("ERROR [*] Could not create Vault client.. %v", err)
	}

	if n, ok := createUserProfile(trans); ok {
		log.Printf("INFO [+] Creates new user with 'id: %v'.", n)
	}

	// Step 5: ----------- Read from DB and Decrypt----------------
	o := orm.NewOrm()
	o.Using("vaultdb")
	user := User{Id: 1}

	switch err = o.Read(&user); {
	case err == orm.ErrNoRows:
		log.Printf("ERROR [*] No result found.. %v", err)
	case err == orm.ErrMissPK:
		log.Printf("ERROR [*] No primary key found.. %v", err)
	case err != nil:
		log.Printf("ERROR [*] Something else went wrong.. %v", err)
	}

	dec, err := trans.Decrypt("beego-key", &vault.TransitDecryptOptions{
		Ciphertext: user.Passkey,
	})
	if err != nil {
		log.Printf("ERROR [*] Decrypt failed.. %v", err)
	}
	log.Printf("INFO [+] Get user(id='%v').Passkey: %#v", user.Id, dec.Data.Plaintext)

	os.Exit(0)
}

func createUserProfile(v *vault.Transit) (int64, bool) {
	answers := []string{
		"It is certain",
		"Without a doubt",
		"Most likely",
	}
	emails := []string{
		"orange.fr",
		"yahoo.co.uk",
		"free.fr",
		"gmx.de",
	}
	balances := randFloats(0.0000, 101.9899, 300)

	// helpers
	seed := time.Now().UTC().UnixNano()
	min := 10
	max := 80
	age := int16(rand.Intn(max-min) + min)
	create := randate()
	nameGenerator := namegenerator.NewNameGenerator(seed)
	name := nameGenerator.Generate()
	o := orm.NewOrm()
	o.Using("vaultdb")

	enc, err := v.Encrypt("beego-key", &vault.TransitEncryptOptions{
		Plaintext: "super_secret",
	})
	if err != nil {
		log.Printf("ERROR [*] Encrypt failed.. %v", err)
	}
	passwd := string(enc.Data.Ciphertext)
	log.Printf("INFO [+] Encrypted: %#v", passwd)

	profile := new(Profile)
	profile.Age = age
	profile.Answer = answers[rand.Intn(len(answers))]
	profile.Balance = balances[rand.Intn(len(balances))]
	profile.Created = create

	user := new(User)
	user.Profile = profile
	user.Name = name
	user.Email = name + "@" + emails[rand.Intn(len(emails))]
	user.Passkey = passwd

	_, err = o.Insert(profile)
	if err != nil {
		return 0, false
	}
	uid, err := o.Insert(user)
	if err != nil {
		return 0, false
	}
	return uid, true
}

// return a random time.Time within a pre-defined period
func randate() time.Time {
	min := time.Date(2015, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	max := time.Date(2021, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	delta := max - min

	sec := rand.Int63n(delta) + min
	return time.Unix(sec, 0)
}

// return list of len n with random floats
func randFloats(min, max float64, n int) []float64 {
	res := make([]float64, n)
	for i := range res {
		res[i] = min + rand.Float64()*(max-min)
	}
	return res
}
