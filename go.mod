module beego-vault

go 1.15

require (
	github.com/astaxie/beego v1.12.3
	github.com/beego/beego/v2 v2.0.1
	github.com/goombaio/namegenerator v0.0.0-20181006234301-989e774b106e
	github.com/hashicorp/vault/api v1.1.1
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/mittwald/vaultgo v0.0.11 // indirect
	github.com/olmax99/vaultgo v0.0.12
	github.com/pkg/errors v0.9.1
)
