package main

import (
	"time"
)

type User struct {
	Id      int
	Name    string
	Email   string   `orm:"unique"`
	Passkey string   `orm:unique`
	Profile *Profile `orm:"rel(one)"` // OneToOne relation
}

type Profile struct {
	Id      int
	Age     int16
	Answer  string
	Created time.Time `orm:"type(datetime);precision(2)"`
	Balance float64   `orm:"digits(12);decimals(4)"`
	User    *User     `orm:"reverse(one)"` // Reverse relationship (optional)
}
